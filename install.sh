#!/bin/bash
git clone https://github.com/rust-analyzer/rust-analyzer.git ~/opt/rust-analyzer
pushd ~/opt/rust-analyzer
cargo xtask install --server
popd
